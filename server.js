let express = require("express");
let bodyParser = require("body-parser");
let fs = require("fs");
let cors = require("cors");

let app = express();
app.use(cors());
app.use(bodyParser.json());

// Create application/x-www-form-urlencoded parser
let urlencodedParser = bodyParser.urlencoded({ extended: false });

////////////////////////////////////////////////
// HELPER FUNCTIONS

// BUG REPORT:
//   Service.json contains bad non-visible characters that break our code
// BUG FIX:
//   Strip off bad characters after reading the file using a regular expression
// NOTES:
//   We need to eventually fix the file so we don't need to do this
function stripBadCharacters(data) {
  data = data.toString();
  data = data.replace(/[^\x20-\x7E]/g, "");
  return data;
}

////////////////////////////////////////////////
// API ENDPOINTS

// GET CATEGORIES
app.get("/api/categories", function (req, res) {
  console.log("Got a GET request for categories");

  let data = fs.readFileSync(__dirname + "/data/categories.json", "utf8");

  // Log returned data for debugging purposes
  data = JSON.parse(data);
  console.log("Returning...");
  console.log(data);

  res.end(JSON.stringify(data));
});

// GET ALL SERVICES
app.get("/api/services", function (req, res) {
  console.log("Got a GET request for ALL services");

  let data = fs.readFileSync(__dirname + "/data/services.json", "utf8");
  data = stripBadCharacters(data); // see helper function above

  // Log returned data for debugging purposes
  data = JSON.parse(data);
  console.log("Returning...");
  console.log(data);

  res.end(JSON.stringify(data));
});

// GET ONE SERVICE BY ID
app.get("/api/services/:id", function (req, res) {
  let id = req.params.id;
  console.log("Got a GET request for service " + id);

  let data = fs.readFileSync(__dirname + "/data/services.json", "utf8");
  data = stripBadCharacters(data); // see helper function above

  data = JSON.parse(data);

  // Find service by id
  let match = data.find((s) => s.ServiceID == id);
  if (match == null) {
    res.status(404).send("Not Found");
    return;
  }

  // Log returned data for debugging purposes
  console.log("Returning...");
  console.log(match);

  res.end(JSON.stringify(match));
});

// GET MANY SERVICES BY CATEGORY ID
app.get("/api/services/bycategory/:id", function (req, res) {
  let id = req.params.id;
  console.log("Got a GET request for services in category " + id);

  // Must read the categories JSON to get the category name
  // NOTE:  Each service contains its category name rather than ID
  let categories = fs.readFileSync(__dirname + "/data/categories.json", "utf8");
  categories = JSON.parse(categories);

  // Find category by CatId
  let selectedCategory = categories.find((c) => c.CatId == id);
  console.log(
    "Id " + id + " matched category " + selectedCategory.CategoryName
  );

  let data = fs.readFileSync(__dirname + "/data/services.json", "utf8");
  data = stripBadCharacters(data); // see helper function above

  data = JSON.parse(data);

  // Find the services offered in a specific by comparing against CategoryName
  let matches = data.filter(
    (s) => s.CategoryName == selectedCategory.CategoryName
  );

  // Log returned data for debugging purposes
  console.log("Returning...");
  console.log(matches);

  res.end(JSON.stringify(matches));
});

// POST A NEW REVIEW OF THE SPA
// The POST message body has a reviewer and comment that is to be processed
// ex:  reviewer=Betty W.&comment=I love this place!
app.post("/api/reviews", function (req, res) {
  console.log("Got a POST request for to add a todo");
  console.log("BODY -------->" + JSON.stringify(req.body));

  let data = fs.readFileSync(__dirname + "/data/reviews.json", "utf8");
  data = JSON.parse(data);

  // Create a review by placing the posted data (reviewer and comment)
  // into an object along with the current date
  let item = {
    date: new Date().toLocaleDateString(), // format as MM/DD/YYYY
    reviewer: req.body.reviewer,
    comment: req.body.comment,
  };

  // Place the new review in the list and re-save the file
  data.push(item);
  fs.writeFileSync(__dirname + "/data/reviews.json", JSON.stringify(data));

  // Log review added for debugging purposes
  console.log("New Review: ");
  console.log(item);

  res.status(201).send();
});

// POST A RESERVATION
// The POST message body has a NAME, PHONE, and DATETIME that is to be processed
app.post("/api/registration", function (req, res) {
  console.log("Got a POST request to add a reservation");
  console.log("BODY -------->" + JSON.stringify(req.body));

  let data = fs.readFileSync(__dirname + "/data/reservation.json", "utf8");
  data = JSON.parse(data);

  // Create a reservation by placing the posted data (NAME, PHONE, and DATETIME)
  // into an object along with the current date
  let item = { 
    name: req.body.name,
    phone: req.body.phone,
	time: req.body.time
  };

  // Place the new review in the list and re-save the file
  data.push(item);
  fs.writeFileSync(__dirname + "/data/reservation.json", JSON.stringify(data));

  // Log review added for debugging purposes
  console.log("New Reservation: ");
  console.log(item);

  res.status(201).send();
});

// GET LAST 3 REVIEWS
app.get("/api/reviews", function (req, res) {
  console.log("Got a GET request for the last 3 reviews");

  let data = fs.readFileSync(__dirname + "/data/reviews.json", "utf8");
  data = JSON.parse(data);

  // Returns the MOST RECENT 3 reviews.  To return more, change the -3 to
  // another number.  For example, -2 returns the last 2 reviews.
  let matching = data.slice(1).slice(-3);

  // Log returned data for debugging purposes
  console.log("Returning...");
  console.log(matching);

  res.end(JSON.stringify(matching));
});

//////////////////////////////////////////////////

app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended: false }));

let server = app.listen(8081, function () {
  let port = server.address().port;
  console.log("App listening at port %s", port);
});
